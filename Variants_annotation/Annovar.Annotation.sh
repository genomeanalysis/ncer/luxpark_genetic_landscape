#!/bin/bash

# /mnt/isilon/projects/ncer-variantdb/scripts/annotation/Annovar.Annotation.sh input_vcf [hg19|hg38]

vcffile=$1
buildver=$2
# mode=$3


threads=10
vcf_name=$(basename $vcffile)
output=$(basename $vcffile | sed -E 's/(.vcf|.vcf.gz)//g')


ANNOVAR=/mnt/isilon/projects/isbsequencing/scripts/ANNOVAR/annovar
ANNOVAR_DB=/mnt/isilon/projects/isbsequencing/scripts/ANNOVAR/humandb

#### hg19
basic_protocol_hg19="refGene,gnomad_genome,gnomad_exome,avsnp150,snp138NonFlagged,clinvar_20221231,hgmd42022,revel,cadd13gt10,caddindel,dann,dbscsnv11,cosmic70,dbnsfp42a,dgv,dgvMerged,mirna,mirnatarget,regsnpintron,condel,spidex,oreganno,rmsk,transfac"
basic_operation_hg19="g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f"


#### hg38
basic_protocol_hg38="refGene,gnomad312_genome,avsnp150,clinvar_20221231,hgmd42022,revel,dbscsnv11,cosmic70,dbnsfp42a"
basic_operation_hg38="g,f,f,f,f,f,f,f,f"


protocol="error"
operation="error"

if [[ $buildver == "hg19" ]]; then
		protocol=$basic_protocol_hg19
		operation=$basic_operation_hg19
		if [[ protocol != "error" ]] & [[ operation != "error" ]] ; then
			if [[ $vcffile == *"vcf"* ]]; then
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver --vcfinput -protocol $protocol -operation $operation --argument '--splicing_threshold 2 --neargene 4000',,,,,,,,,,,,,,,,,,,,,,, -remove -polish -outfile $output.annotated -nastring . --otherinfo

			else
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver -protocol $protocol -operation $operation --argument '--splicing_threshold 2 --neargene 4000',,,,,,,,,,,,,,,,,,,,,,, -remove -polish -outfile $output.annotated -nastring . --otherinfo
			fi
		else
			echo "ERROR on arguments"
		fi
	fi


if [[ $buildver == "hg38" ]]; then
	ANNOVAR_DB=/mnt/isilon/projects/isbsequencing/scripts/ANNOVAR/humandb/hg38
	protocol=$basic_protocol_hg38
	operation=$basic_operation_hg38
		if [[ protocol != "error" ]] & [[ operation != "error" ]] ; then
			if [[ $vcffile == *"vcf"* ]]; then
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver --vcfinput -protocol $protocol -operation $operation --argument '--splicing_threshold 2 --neargene 4000',,,,,,, -remove -polish -outfile $output.annotated -nastring .

			else
				perl $ANNOVAR/table_annovar.pl $vcffile $ANNOVAR_DB --thread $threads --buildver $buildver -protocol $protocol -operation $operation --argument '--splicing_threshold 2 --neargene 4000',,,,,,, -remove -polish -outfile $output.annotated -nastring . --otherinfo
			fi
		else
			echo "ERROR on arguments"
		fi
	fi



echo "END ANNOVAR ANNOTATION"

