# Title: Genetic landscape of Parkinson’s disease in Luxembourg

## Study description

- [ ] A descriptive genetic characterization of the deeply phenotyped Luxembourgish PD case-control cohort: screening for rare SNVs, rare CNVs and low-risk SNPs effects
- [ ] The Luxembourg Parkinson’s Study, a large longitudinal monocentric observational study in the framework of the NCER-PD (National Centre for Excellence in Research in Parkinson’s Disease) project, comprised 783 patients (680 typical PD and 103 atypical PD) and 809 Healthy Controls.

## Genotyping and Clinicial dataset
- [ ] DNA samples of participants from the Luxembourg Parkinson’s study and COURAGE-PD genotyped using the Neurochip array (v.1.0 and v1.1; Illumina, San Diego, CA) 
- [ ] Clinical outcome: Age at onset (AAO) set as age at PD diagnosis.

## genotyping Quality control (QC)
QC steps of genotyping data was performed according to the standard procedures (See genotyping_QC directory) using:
- Plink v1.90b6.18
- Vcftools v0.1.16
- Tabix v1.9
- Bcftools v1.9 
- KING v1.4
- In-house python and R-scripts.

## Variant annotation
- ANNOVAR (v 2020-06-08)

## Copy number variant calling
- Generation of intensity file using GenomeStudio (v2.0.5 Illumina)
- CNV calling using PennCNV (v1.0.5)

## Polygenic Risk Scores (PRSs)
PRSs were calculated using PRSice2 v2.3.3 (https://choishingwan.github.io/PRSice/):
- Base file for PRS calculation: 90 SNPs from Nalls et al 2019 (10.1016/S1474-4422(19)30320-5)
- Base file for gene-set analysis: summary statistics of PD GWAS Nalls et al 2019 (10.1016/S1474-4422(19)30320-5)
- Target file: LuxPark genotyped data after QC and imputation (plink format)
- MSIG file: 3090 canonical pathways “c2.cp.v2023.1.Hs.symbols.gmt” downloaded from the molecular signature database (MSigDB, https://www.gsea-msigdb.org/)
- Mapping file: Homo_sapiens.GRCh37.87.gtf download from ENSMBL (https://grch37.ensembl.org/info/data/ftp/index.html)

