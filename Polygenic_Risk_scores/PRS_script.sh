#!/bin/bash
module load lang/R
## script to calculate PRS using PRSice2 (https://choishingwan.github.io/PRSice/)
## Base file: 90 SNPs from Nalls et al 2019 (DOI:10.1016/S1474-4422(19)30320-5)
## Target file: LuxPark genotyped data after qc and imputation (plink format)

Rscript ~/PRSice_linux_/PRSice.R --dir . --prsice ~/PRSice_linux_/PRSice_linux  --binary-target T --or --pvalue P --stat OR --thread 8 --upper 0.5 --base 90SNPs_Nalls_PD_GWAS --target plink_file --print-snp --out output_files

## script to calculate gene-sets PRS using PRSice2 (https://choishingwan.github.io/PRSice/)
## Base file: summary statistics of PD GWAS Nalls et al 2019 (10.1016/S1474-4422(19)30320-5)
## Target file: LuxPark genotyped data after qc and imputation (plink format)
## gtf files: Homo_sapiens.GRCh37.87.gtf.gz
## msigdb file: c2.cp.v2023.1.Hs.symbols.gmt

Rscript ~/PRSice_linux_/PRSice.R --dir . --prsice ~/PRSice_linux_/PRSice_linux --binary-target T --beta --pvalue p --stat b --thread 32 --seed 1234 --ultra --maf 0.01 --clump-kb 250 --base Nalls_PD_GWAS --target plink_file --gtf Homo_sapiens.GRCh37.87.gtf.gz --msigdb c2.cp.v2023.1.Hs.symbols.gmt --out output_files --proxy 0.8 --upper 0.05