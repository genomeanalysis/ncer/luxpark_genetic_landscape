#!/usr/bin/python
import os,sys
infile=open(sys.argv[1], "r")

name=[]
genotype=[]

outfile_1=open(sys.argv[1].replace(".vcf", ".clean.vcf"), "w")
count = 0

for a in infile:
        if not a.startswith("#"):
                count+=1
#                all=sum(1 for _ in a)
                print(str(count), end="\r")
                a=a.rstrip().split()
                name.append(a[0]+"_"+a[1]+"_"+a[0]+":"+a[1]+":"+a[3]+":"+a[4]+"_"+a[3]+"_"+a[4]+"_"+"_".join(a[5:9]))
                genotype.append(a[9:])
        else:
                outfile_1.write(a)

name_dict=dict()
name_list=zip(name, genotype)

for line in name_list:
        if line[0] in name_dict:
        # append the new number to the existing array at this slot
                name_dict[line[0]].append(line[1])
        else    :
        # create a new array in this slot
                name_dict[line[0]] = [line[1]]

outfile_2=open("multiple_ids_per_variant.tsv", "w")
for key, value in name_dict.items():
        if len(value)>1:
                outfile_1.write("\t".join(key.split("_"))+"\t"+"\t".join([[k for k in list(set(['1/1' for m in set(list(l))])) if k!='0/0'][0] if len(set(list(l))) > 1 else list(set(list(l)))[0] for l in zip(*value)])+"\n")

                outfile_2.write(key+"\t"+"\t".join([[k for k in list(set(['1/1' for m in set(list(l))])) if k!='0/0'][0] if len(set(list(l))) > 1 else list(set(list(l)))[0] for l in zip(*value)])+"\n")
                #print(key, [[k for k in list(set([m for m in set(list(l))]))] if len(set(list(l))) > 1 else list(set(list(l)))[0] for l in zip(*value)])
        else:
                outfile_1.write("\t".join(key.split("_"))+"\t"+"\t".join(value[0])+"\n")
                #print(key, "\t".join(value[0]))
