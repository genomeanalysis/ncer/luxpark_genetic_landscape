#!/usr/bin/python
# -*- coding: utf-8 -*-
#This script is used to add chr to the beginning of chromosome numbers
import os,sys,gzip,codecs,hashlib
import json

vcf=sys.argv[1]

if vcf.endswith(".vcf.gz"):
    infile=gzip.open(vcf, "rt")
elif vcf.endswith(".vcf"):
    infile=codecs.open(vcf, "r")

outfile=open(vcf+".fliplist.txt", "w")

for a in infile:
	if not a.startswith("#"):
		a=a.rstrip().split()
		if "," in a[4]:
			outfile.write(a[2]+"\n")



