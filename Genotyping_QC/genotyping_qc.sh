#!/bin/bash
###Script to QC and flip Genotyping data 21/10/2021
###Input can be .vcf or any plink file
##plink file must have same IID and FID (need to be fixed soon)

module load lang/R
source /mnt/irisgpfs/users/zlandoulsi/scripts/source_file.txt
infile=$1
script_dir=/home/users/zlandoulsi/scripts/qc_flip
infile2=$(echo "${infile%.*}")
filename=$(basename $infile2)
#echo "$filename"
qc.step ()
{
        if [[ $infile == *.vcf ]]
                then
                plink --vcf $infile --double-id --make-bed --out $filename
        elif [[ $infile == *.ped || $infile == *.map ]]
        then
                plink --file $filename --make-bed --out $filename
        fi

        ##QC 1st round
        plink --bfile $infile2 --make-bed --out $filename.tmp
        plink --bfile $filename.tmp --mac 1 --geno 0.05 --mind 0.05 --chr 1-22,X,Y --hwe 1e-6 --snps-only just-acgt --make-bed --out $filename.qc1
        plink --bfile $filename.qc1 --missing --out $filename.qc1
        rm $filename.tmp.*
        echo '########first QC done############'
}

infer.relationship ()
{
        ##infer relationship KING
        /mnt/irisgpfs/projects/isbsequencing/tools/KING/king -b $filename.qc1.bed --kinship --ibs --prefix $filename.qc1
        python $script_dir/infer_relations_king.py $filename.qc1.ibs0 > $filename.qc1.infered.relationships_king.txt
        awk '{if ($4 == "1st-degree") print $1,$2}' $filename.qc1.infered.relationships_king.txt > $filename.1st_degree_sample_list
        Rscript $script_dir/Remove_high_miss_related_samples.R $filename.1st_degree_sample_list $filename.qc1.imiss
	#output: rel_ind_to_remove.txt
	mv rel_ind_to_remove.txt $filename.rel_ind_to_remove.txt
        plink --bfile $filename.qc1 --remove $filename.rel_ind_to_remove.txt --make-bed --out $filename.qc1.rel
	rm $filename.qc1.bed $filename.qc1.bim $filename.qc1.fam $filename.qc1.log $filename.qc1.nosex
	rm $filename.qc1.ibs $filename.qc1.ibs0 $filename.qc1.imiss $filename.qc1.lmiss $filename.qc1.infered.relationships_king.txt
	rm $filename.qc1TMP.ped $filename.qc1TMP.dat
        echo '#######Infering relationship################'
}

het.rate ()
{
        ##remove excess of heterozygosity
        plink --bfile $filename.qc1.rel --het --out $filename.qc1.rel
        Rscript $script_dir/Heterozygosity_outliers_list.R $filename.qc1.rel.het
        #output fail_het_qc.txt
	mv fail_het_qc.txt $filename.fail_het_qc.txt
	plink --bfile $filename.qc1.rel --remove $filename.fail_het_qc.txt --make-bed --out $filename.qc1.rel.het
	rm $filename.qc1.rel.bed $filename.qc1.rel.bim $filename.qc1.rel.fam $filename.qc1.rel.log $filename.qc1.rel.nosex
	rm $filename.qc1.rel.het $filename.qc1.rel.het.log $filename.qc1.rel.het.nosex
	echo '##########Heterozygosity rate checked################'
        plink --bfile $filename.qc1.rel.het --recode vcf-iid --output-chr M --out $filename.qc --snps-only just-acgt
}

flip.step ()
{
	#split vcf in chr (to avoid out of memory error)
	seq 22 | parallel "vcftools --vcf $filename.qc.vcf --chr {} --recode --recode-INFO-all --out chr{}.$filename.qc.vcf"
	### Collapse duplicate variants to single line
	seq 22 | parallel "python $script_dir/collapse_variants.py chr{}.$filename.qc.vcf.recode.vcf"
	seq 22 | parallel "rm chr{}.$filename.qc.vcf.recode.vcf"
	ls chr[0-9]* | sort -V > list.txt
	#output $filename.qc.clean.vcf
	bcftools concat --output-type v --output $filename.qc.clean.vcf --file-list list.txt
	rm chr[0-9]*
	rm list.txt multiple_ids_per_variant.tsv
	mkdir -p temp
	cat $filename.qc.clean.vcf | vcf-sort -t temp | bgzip -c > $filename.clean.vcf.gz
	tabix -fp vcf $filename.clean.vcf.gz
	
	###Convert the ref alt alleles according to reference genome, but this also is generating random positions which needs to be swapped from top to bottom strand
	bcftools norm --check-ref ws -f /work/projects/isbsequencing/data/human/genomes/GRCh37/Homo_sapiens.GRCh37.dna.primary_assembly.fa $filename.clean.vcf.gz -o $filename.clean.2.vcf -Ov

	### Get the list of top bottom problematic positions from file above
	python $script_dir/print_prob_snps.py $filename.clean.2.vcf
	#output $filename.clean.2.vcf.fliplist.txt

	### Flip the top and bottom strands
	plink --vcf $filename.clean.vcf.gz --double-id --make-bed --out $filename.clean.3
	plink --bfile $filename.clean.3 --flip $filename.clean.2.vcf.fliplist.txt --output-chr M --recode vcf-iid --out $filename.clean.4
	cat $filename.clean.4.vcf | vcf-sort -t temp | bgzip -c > $filename.clean.4.vcf.gz
	tabix -fp vcf $filename.clean.4.vcf.gz

	### After fixing the top and bottom strand now the ref alt alleles are changed acocording to the reference fasta
	bcftools norm --check-ref ws -f /work/projects/isbsequencing/data/human/genomes/GRCh37/Homo_sapiens.GRCh37.dna.primary_assembly.fa $filename.clean.4.vcf.gz -o $filename.final.qc.flip.vcf -Ov 
	rm $filename.clean.* $filename.qc*
	rm multiple_ids_per_variant.tsv
	rm -fr temp
	echo '########flip step done##########'
}

generate.pc ()
{
	plink --vcf $filename.final.qc.flip.vcf --double-id --make-bed --out $filename.final.qc.flip	
	plink --bfile $filename.final.qc.flip --pca 10 --out $filename.final.qc.flip
	echo '#########pca generated##########'
}

generate.MDS.plot ()
{
	##filter for maf and independent pruned SNPs
	plink --bfile $filename.final.qc.flip --indep-pairwise 50 5 0.2 --out $filename.final.qc.flip
	plink --bfile $filename.final.qc.flip --extract $filename.final.qc.flip.prune.in --maf 0.05 --allow-no-sex --make-bed --out $filename.final.qc.flip.1
	##Format SNPs ids
	awk -v OFS="\t" '{print $1,"chr"$1":"$4":"$5,$3,$4,$5,$6}' $filename.final.qc.flip.1.bim  > $filename.final.qc.flip.1.bim2
	mv $filename.final.qc.flip.1.bim2 $filename.final.qc.flip.1.bim
	##Extract the variants present in dataset from the 1000 genomes dataset.
	awk '{print$2}' $filename.final.qc.flip.1.bim > $filename.final.qc.flip.1.SNPs.txt
	plink --bfile /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC --extract $filename.final.qc.flip.1.SNPs.txt --make-bed --out /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.1
	##Extract the variants present in 1000 Genomes dataset from the HapMap dataset.
	awk '{print$2}' /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.1.bim > /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.1.SNPs.txt
	plink --bfile $filename.final.qc.flip.1 --extract /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.1.SNPs.txt --recode --make-bed --out $filename.final.qc.flip.2

	##The datasets must have the same build. Change the build 1000 Genomes data build.
	awk '{print$2,$4}' $filename.final.qc.flip.2.map > $filename.final.qc.flip.buildhapmap.txt
	##buildhapmap.txt contains one SNP-id and physical position per line.
	plink --bfile /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.1 --update-map $filename.final.qc.flip.buildhapmap.txt --make-bed --out /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.2

	## Merge the HapMap and 1000 Genomes data sets
	# 1) set reference genome
	awk '{print$2,$5}' /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.2.bim > /home/users/zlandoulsi/scripts/1000G_plink/1kg_ref-list.txt
	plink --bfile $filename.final.qc.flip.2 --reference-allele /home/users/zlandoulsi/scripts/1000G_plink/1kg_ref-list.txt --make-bed --out $filename.final.qc.flip.3
	# The 1kG_MDS7 and the HapMap-adj have the same reference genome for all SNPs.
	# This command will generate some warnings for impossible A1 allele assignment.

	# 2) Resolve strand issues.
	# Check for potential strand issues.
	awk '{print$2,$5,$6}' /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.2.bim > /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.2.tmp
	awk '{print$2,$5,$6}' $filename.final.qc.flip.3.bim > $filename.final.qc.flip.3.tmp
	sort /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.2.tmp $filename.final.qc.flip.3.tmp | uniq -u > $filename.final.qc.flip.3.all_differences.txt
	# differences between the files, some of these might be due to strand issues.

	##Flip SNPs for resolving strand issues.
	# Print SNP-identifier and remove duplicates.
	awk '{print$1}' $filename.final.qc.flip.3.all_differences.txt | sort -u > $filename.final.qc.flip.3.flip_list.txt
	# Flip the non-corresponding SNPs.
	plink --bfile $filename.final.qc.flip.3 --flip $filename.final.qc.flip.3.flip_list.txt --reference-allele /home/users/zlandoulsi/scripts/1000G_plink/1kg_ref-list.txt --make-bed --out $filename.final.qc.flip.4

	# Check for SNPs which are still problematic after they have been flipped.
	awk '{print$2,$5,$6}' $filename.final.qc.flip.4.bim > $filename.final.qc.flip.4.tmp
	sort /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.2.tmp $filename.final.qc.flip.4.tmp |uniq -u  > $filename.final.qc.flip.4.uncorresponding_SNPs.txt
	# This file demonstrates that there are still differences between the files.

	# 3) Remove problematic SNPs from Dataset and 1000 Genomes.
	awk '{print$1}' $filename.final.qc.flip.4.uncorresponding_SNPs.txt | sort -u > $filename.final.qc.flip.4.SNPs_for_exlusion.txt
	# The command above generates a list of the SNPs which caused the differences between the dataset and the 1000 Genomes data sets after flipping and setting of the reference genome.

	# Remove the problematic SNPs from both datasets.
	plink --bfile $filename.final.qc.flip.4 --exclude $filename.final.qc.flip.4.SNPs_for_exlusion.txt --make-bed --out $filename.final.qc.flip.5
	plink --bfile /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.2 --exclude $filename.final.qc.flip.4.SNPs_for_exlusion.txt --make-bed --out /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.3

	# Merge HapMap with 1000 Genomes Data.
	plink --bfile $filename.final.qc.flip.5 --bmerge /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.3.bed /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.3.bim /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.3.fam --allow-no-sex --make-bed --out $filename.final.qc.flip.MDS_merged

	# Perform MDS
	plink --bfile $filename.final.qc.flip.MDS_merged --genome --out $filename.final.qc.flip.MDS_merged
	plink --bfile $filename.final.qc.flip.MDS_merged --read-genome $filename.final.qc.flip.MDS_merged.genome --cluster --mds-plot 10 --out $filename.final.qc.flip.MDS_merged

	#Create a racefile of your own data.
	awk -v awkvar="$filename" '{print$1,$2,awkvar}' $filename.final.qc.flip.5.fam > $filename.final.qc.flip.racefile.txt
	# Concatenate racefiles (1000G racefile formatted prealably)
	cat /mnt/irisgpfs/users/zlandoulsi/scripts/1000G_plink/race_1kg_final.txt $filename.final.qc.flip.racefile.txt | sed -e '1i\FID IID race' > $filename.final.qc.flip.1kg.racefile.txt
	rm $filename.final.qc.flip.[0-9].*
	rm /home/users/zlandoulsi/scripts/1000G_plink/1kG_MDS_QC.[0-9].*
	rm $filename.final.qc.flip.prune.*

	# Generate population stratification plot.
	Rscript $script_dir/MDS_plot.R $filename.final.qc.flip.MDS_merged.mds $filename.final.qc.flip.1kg.racefile.txt
}

####
qc.step
infer.relationship
het.rate
flip.step
generate.pc
generate.MDS.plot
